<?php

namespace App\Repository;

use App\User;
use App\Message;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collections;

class ConversationRepository
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Messaage
     */
    private $message;

    /**
     * @param User $user
     * @param Message $message
     */
    public function __construct(User $user, Message $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Получение всех собеседников
     *
     * @param integer $userId
     * @return Collection
     */
    public function getConversations(int $userId) : Collection
    {
        return $this->user->newQuery()
            ->select('id', 'name')
            ->where('id', '!=', $userId)
            ->get();
    }

    /**
     * Create new message
     *
     * @param string $content
     * @param integer $from
     * @param integer $to
     * @return Message
     */
    public function createMessage(string $content, int $from, int $to) : Message
    {
        return $this->message->newQuery()
            ->create([
                'content' => $content,
                'from_id' => $from,
                'to_id' => $to,
                'created_at' => Carbon::now()
            ]);
    }

    /**
     * Get all messages for src/dst users
     *
     * @param integer $from_id
     * @param integer $to_id
     * @return Builder
     * @todo Оптимизировать whereRaw()
     */
    public function getMessagesFor(int $from_id, int $to_id) : Builder
    {
        return $this->message->newQuery()
            ->with(['from' => function ($query) {
                return $query->select('id', 'name');
            }])
            ->whereRaw("(from_id = $from_id AND to_id = $to_id OR from_id = $to_id AND to_id = $from_id)")
            ->orderBy('created_at', 'DESC');
    }

    /**
     * Get count the unreaded messages
     *
     * @param integer $userId
     * @return \Collections
     */
    public function unreadCount(int $userId) : Collections
    {
        return $this->message->newQuery()
            ->selectRaw('from_id, COUNT(*) AS count')
            ->where('to_id', $userId)
            ->whereRaw('read_at IS NULL')
            ->groupBy('from_id')
            ->get()
            ->pluck('count', 'from_id');
    }

    /**
     * Mark messages as read
     *
     * @param integer $from
     * @param integer $to
     * @return void
     */
    public function readAllFrom(int $from, int $to)
    {
        $this->message
            ->where('from_id', $from)
            ->where('to_id', $to)
            ->update([
                'read_at' => Carbon::now()
            ]);
    }
}
