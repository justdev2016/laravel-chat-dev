<?php

namespace App\Http\Controllers\Api;

use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
    /**
     * Mark message as read
     *
     * @param Request $request
     * @param Message $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Request $request, Message $message)
    {
        $message->update([
            'read_at' => Carbon::now()
        ]);

        return response()
            ->json([
                'success' => 1
            ]);
    }
}
