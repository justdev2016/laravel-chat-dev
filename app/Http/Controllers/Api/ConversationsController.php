<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\NewMessageEvent;
use App\Http\Controllers\Controller;
use App\Repository\ConversationRepository;
use App\Http\Requests\StoreMessageRequest;

class ConversationsController extends Controller
{
    /**
     * @var ConversationRepository
     */
    private $conversationRepository;
    
    /**
     * @param ConversationRepository $repository
     */
    public function __construct(ConversationRepository $repository)
    {
        $this->conversationRepository = $repository;
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $conversations = $this->conversationRepository->getConversations(auth()->id());
        $unread = $this->conversationRepository->unreadCount(auth()->id());

        foreach ($conversations as $conversation) {
            $conversation->unread = $unread[$conversation->id] ?? 0;
        }

        return response()
            ->json(compact('conversations'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user)
    {
        $messagesQuery = $this->conversationRepository
            ->getMessagesFor($request->user()->id, $user->id);

        $count = null;
        $update = false;
            
        if ($request->has('before')) {
            $messagesQuery = $messagesQuery->where('created_at', '<', $request->get('before'));
        } else {
            $count = $messagesQuery->count();
        }

        $messages = $messagesQuery->limit(10)->get();

        foreach ($messages as $message) {
            if ($message->read_at === null && $message->to_id === $request->user()->id) {
                $message->read_at = Carbon::now();

                if ($update === false) {
                    $this->conversationRepository->readAllFrom($message->from_id, $message->to_id);
                }

                $update = true;
                break;
            }
        }

        $messages = array_reverse($messages->toArray());

        return response()
            ->json(compact('messages', 'count'));
    }

    /**
     * @param User $user
     * @param StoreMessageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(User $user, StoreMessageRequest $request)
    {
        $message = $this->conversationRepository->createMessage(
            $request->get('content'),
            $request->user()->id,
            $user->id
        );

        broadcast(new NewMessageEvent($message));

        return response()
            ->json(compact('message'));
    }
}
