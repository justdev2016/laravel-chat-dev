<?php

namespace App\Http\Controllers;

use App\User;
use App\Repository\ConversationRepository;
use App\Http\Requests\StoreMessageRequest;
use App\Notifications\MessageReceived;

class ConversationsController extends Controller
{
    /**
     * @var ConversationRepository
     */
    private $conversationRepository;

    /**
     * @param ConversationRepository $repository
     */
    public function __construct(ConversationRepository $repository)
    {
        $this->middleware('auth');

        $this->conversationRepository = $repository;
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $users  = $this->conversationRepository->getConversations(auth()->id());
        $unread = $this->conversationRepository->unreadCount(auth()->id());

        return view('conversations.index', compact('users', 'unread'));
    }

    /**
     * @param User $user
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show(User $user)
    {
        $users    = $this->conversationRepository->getConversations(auth()->id());
        $messages = $this->conversationRepository->getMessagesFor(auth()->id(), $user->id)->paginate(50);
        $unread   = $this->conversationRepository->unreadCount(auth()->id());

        if (isset($unread[$user->id])) {
            $this->conversationRepository->readAllFrom($user->id, auth()->id());
            unset($unread[$user->id]);
        }

        return view('conversations.show', compact('users', 'user', 'messages', 'unread'));
    }

    /**
     * @param User $user
     * @param StoreMessageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(User $user, StoreMessageRequest $request)
    {
        $message = $this->conversationRepository->createMessage(
            $request->get('content'),
            $request->user()->id,
            $user->id
        );

        $this->notify(new MessageReceived($message));

        return redirect()->route('conversations.show', ['id' => $user->id]);
    }
}
