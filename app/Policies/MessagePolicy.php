<?php

namespace App\Policies;

use App\User;
use App\Message;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Message $message
     * @return bool
     */
    public function read(User $user, Message $message)
    {
        return $user->id === $message->to_id;
    }
}
