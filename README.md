## Laravel Chat Server

1. Получить код

```bash
git clone https://justdev2016@bitbucket.org/justdev2016/laravel-chat-dev.git
```

2. Установка зависимостей для PHP

```bash
composer install
```
3. Установка зависимостей для JavaScript

```bash
npm -i
```
или
```bash
yarn
```

3. Генерация ключей для `laravel/passport`

```bash
php artisan passport:keys
```

4. Создание таблиц и заполнение тестовыми данными

```bash
php artisan make:refresh --seed
```

5. Установить `laravel-echo-server` глобально

```bash
npm install -g laravel-echo-server
```

6. Запустить `laravel-echo-server`

```bash
laravel-echo-server start
```

7. Запустить сервер локально

```bash
php artisan serve
```

8. Запустить сборку с автообновлением страницей

```bash
npm hot
```
или
```bash
yarn hot
```