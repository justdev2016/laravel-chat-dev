import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import Messager from './components/Messager'
import store from './store'
import router from './router'
import { messager } from './support'

window.io = require('socket.io-client')

if (messager) {
    // proxy router to store
    sync(store, router)

    new Vue({
        el: '#app',
        components: { Messager },
        store,
        router
    })

}
