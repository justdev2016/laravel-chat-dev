import Favico from 'favico.js'

const token = document.head.querySelector('meta[name="csrf-token"]')
const messager = document.querySelector('#messager')
const favicon = new Favico({ animation: 'none' })
const audio = new Audio('/notification.mp3')
const title = document.title

/**
 * Update Document Title after get new message
 * 
 * @param {object} conversations 
 */
const updateTitle = conversations => {
    let unread = Object.values(conversations).reduce((acc, conversation) => conversation.unread + acc, 0)

    if (unread === 0) {
        document.title = title
        favicon.reset()
    } else {
        document.title = `(${unread}) ${title}`
        favicon.badge(unread)
    }
}

export {
    token,
    messager,
    audio,
    updateTitle
}