import Vue from 'vue'
import VueRouter from 'vue-router'
import Messages from './views/Messages'
import { messager } from './support'

Vue.use(VueRouter)

const routes = [
    {
        path: '/'
    }, {
        path: '/:id',
        name: 'conversation',
        component: Messages
    }, {
        path: '*',
        redirect: '/'
    }
]

export default new VueRouter({
    mode: 'history',
    routes,
    base: messager ? messager.getAttribute('data-base') : '/'
})