import axios from 'axios'
import { token } from './support'

/** Default axios settings */
const $http = axios.create({
    baseURL: 'http://laravel-chat.loc/api/',
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': token.content
    }
})

class Api {
    /**
     * @param {string} url
     * @param {object} params
     * @todo включить поддержку обработки ошибок в компонентах
     */
    async get(url, params = {}) {
        try {
            let { data } = await $http.get(url, params)
            
            return data
        } catch ({ response }) {
            // throw await response.data
            console.error(response.data)
        }
    }
    /**
     * @param {string} url
     * @param {object} params
     */
    async post(url, params = {}) {
        try {
            let { data } = await $http.post(url, params)

            return data
        } catch ({ response }) {
            throw await response.data
        }
    }
}
 
export default new Api()