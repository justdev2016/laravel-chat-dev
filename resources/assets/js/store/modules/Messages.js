import Echo from 'laravel-echo'
import { updateTitle, audio } from '../../support'
import Api from '../../api'
import * as types from '../types'

/**
 * {
 *      1: {
 *          id: 1,
 *          name: 'John Doe 1',
 *          unread: 0,
 *          count: 100,
 *          messages: [{
 *              id,
 *              from_id
 *              to_id...
 *          }]
 *      }
 * }
 */

/** Stete */
const state = {
    openedConversations: [],
    conversations: {}
}
/** Getters */
const getters = {
    conversations: state => state.conversations,
    messages: (state, getters, rootState) => {
        let id = rootState.route.params.id
        let conversation = state.conversations[id]

        return (conversation && conversation.messages)
            ? conversation.messages
            : []
    },
    conversation: (state, getters, rootState) => state.conversations[rootState.route.params.id] || {},
    conversationLoaded: (state, getters) => getters.conversation.loaded
}
/** Mutations */
const mutations = {
    [types.MARK_AS_READ](state, id) {
        state.conversations[id].unread = 0
    },
    [types.ADD_CONVERSATIONS](state, { conversations }) {
        conversations.forEach(c => {
            let conversation = state.conversations[c.id] || { messages: [], count: 0 }

            conversation = {...conversation, ...c }

            state.conversations = {...state.conversations,
                ... {
                    [c.id]: conversation
                }
            }
        })
    },
    [types.ADD_MESSAGES](state, { id, messages, count }) {
        let conversation = state.conversations[id] || {}

        conversation.messages = messages
        conversation.count = count
        conversation.loaded = true

        state.conversations = {...state.conversations,
            ... {
                [id]: conversation
            }
        }
    },
    [types.ADD_MESSAGE](state, { id, message }) {
        state.conversations[id].count++
        state.conversations[id].messages.push(message)
    },
    [types.PREPEND_MESSAGES](state, { id, messages }) {
        let conversation = state.conversations[id] || {}

        conversation.messages = [...messages, ...conversation.messages]

        state.conversations = {...state.conversations,
            ... {
                [id]: conversation
            }
        }
    },
    [types.OPEN_CONVERSATION](state, id) {
        state.openedConversations = [id]
    },
    [types.INCREMENT_UNREAD](state, id) {
        let conversation = state.conversations[id]

        if (conversation) {
            conversation.unread++
        }
    },
    [types.READ_MESSAGE](state, message) {
        let conversation = state.conversations[message.from_id]

        if (conversation && conversation.messages) {
            let msg = conversation.messages.find(m => m.id === message.id)

            if (msg) {
                msg.read_at = (new Date()).toISOString()
            }
        }
    }
}
/** Actions */
const actions = {
    /**
     * Init new mesagge socket event listener
     * @param {object} param0 
     * @param {number} userId 
     */
    initSocketEventListener({ commit, state, dispatch }, userId) {
        new Echo({
            broadcaster: 'socket.io',
            host: `${window.location.hostname}:6001`
        })
            .private(`App.User.${userId}`)
            .listen('NewMessageEvent', event => {
                commit(types.ADD_MESSAGE, { id: event.message.from_id, message: event.message })

                if (!state.openedConversations.includes(event.message.from_id) || document.hidden) {
                    commit(types.INCREMENT_UNREAD, event.message.from_id)

                    audio.play()

                    updateTitle(state.conversations)
                } else {
                    dispatch('markAsRead', event.message)
                }
            })
    },
    /**
     * Load conversations list
     * @param {object} param0 
     */
    async loadConversations({ commit }) {
        let response = await Api.get('conversations')

        commit(types.ADD_CONVERSATIONS, { ...response })
    },
    /**
     * Load messages for current conversation
     * @param {object} param0 
     */
    async loadMessages({ commit, getters, dispatch, rootState }) {
        let conversationId = rootState.route.params.id
        let id = parseInt(rootState.route.params.id, 10)

        commit(types.OPEN_CONVERSATION, id)

        if (!getters.conversationLoaded) {
            let response = await Api.get(`conversations/${conversationId}`)

            commit(types.ADD_MESSAGES, { id: conversationId, ...response })
        }
        getters.messages.forEach(message => {
            if (message.read_at === null && message.to_id === rootState.Account.user) {
                dispatch('markAsRead', message)
            }
        })
        commit(types.MARK_AS_READ, conversationId)
        updateTitle(state.conversations)
    },
    /**
     * Send new message on server
     * @param {object} param0 
     * @param {string|null} content 
     */
    async sendMessage({ commit, rootState }, content) {
        let userId = rootState.route.params.id
        let response = await Api.post(`conversations/${userId}`, {
            content: content
        })

        commit(types.ADD_MESSAGE, { id: userId, ...response })
    },
    /**
     * Load previouse messages after scrollTop
     * @param {object} param0 
     */
    async loadPreviousMesssages({ commit, getters, rootState }) {
        let conversationId = rootState.route.params.id
        let message = getters.messages[0]

        if (message) {
            let url = `conversations/${conversationId}?before=${message.created_at}`
            let response = await Api.get(url)

            commit(types.PREPEND_MESSAGES, { id: conversationId, ...response })
        }
    },
    /**
     * Send on server the readed message
     * @param {object } param0 
     * @param {object} message 
     */
    markAsRead({ commit }, message) {
        Api.post(`messages/${message.id}`)

        commit(types.READ_MESSAGE, message)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}