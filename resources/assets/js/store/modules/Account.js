import { audio, updateTitle } from '../../support'
import * as types from '../types'

/** Stete */
const state = {
    user: null
}
/** Getters */
const getters = {
    user: state => state.user
}
/** Mutations */
const mutations = {
    [types.SET_USER](state, id) {
        state.user = id
    }
}
/** Actions */
const actions = {
    setUser({ commit, dispatch }, id) {
        commit(types.SET_USER, id)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}