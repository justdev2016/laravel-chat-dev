@extends('layouts.app')

@section('content')

    <div class="container">

        <div id="messager" data-base="{{ route('conversations', [], false) }}">
            <messager :user="{{ auth()->id() }}"></messager>
        </div>

    </div>

@endsection