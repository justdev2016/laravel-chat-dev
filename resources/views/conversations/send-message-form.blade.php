{{--  @include('errors.show-errors')  --}}

<form action="" method="POST">
    {{ csrf_field() }}
    <div class="form-group">
        <textarea name="content" placeholder="Введите сообщение" class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}"></textarea>
        
        @if ($errors->has('content'))
            <div class="invalid-feedback">
                {{ implode(',', $errors->get('content')) }}
            </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Send</button>
</form>