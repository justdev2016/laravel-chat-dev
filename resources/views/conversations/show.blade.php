@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @include('conversations.users', ['users' => $users, 'unread' => $unread])

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ $user->name }}</div>

                    <div class="card-body conversations">
                        @if ($messages->hasMorePages())
                            <div class="text-center">
                                <a href="{{ $messages->nextPageUrl() }}" class="btn btn-light">Получить следующие сообщения</a>
                            </div>
                        @endif
                        @foreach (array_reverse($messages->items()) as $message)
                            <div class="row">
                                <div class="col-md-10 {{ $message->from->id !== $user->id ? 'offset-md-2 text-right' : '' }}">
                                    <p>
                                        <strong>
                                            {{ $message->from->id !== $user->id ? 'Я' : $message->from->name }}
                                        </strong>
                                        <br/>
                                        {!! nl2br(e($message->content)) !!}
                                    </p>
                                </div>
                            </div>
                            <hr/>
                        @endforeach

                        @if ($messages->previousPageUrl())
                            <div class="text-center">
                                <a href="{{ $messages->previousPageUrl() }}" class="btn btn-light">Получить предыдущие сообщения</a>
                            </div>
                        @endif
                        
                        @include('conversations.send-message-form')
                    </div><!-- END .card-body -->

                </div><!-- END .card -->
            </div>
        </div>
    </div>

@endsection