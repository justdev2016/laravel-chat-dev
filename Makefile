.PHONY=dev

dev:
	tmux \
	  new-session "php artisan serve" \;\
	  split-eindow -h "yarn hot" \;\
	  split-eindow -v "laravel-echo-server start" \;\