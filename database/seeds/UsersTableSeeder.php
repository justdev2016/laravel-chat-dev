<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [];
        
        for ($i = 0; $i < 10; $i++) {
            $users[] = [
                'name' => "JohnDoe{$i}",
                'email' => "john{$i}@doe.ua",
                'password' => bcrypt('0000')
            ];
        }
        
        \DB::table('users')->insert($users);
    }
}
