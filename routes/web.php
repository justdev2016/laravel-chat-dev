<?php

Route::view('/', 'welcome');

Auth::routes();

Route::view('/home', 'home')->name('home');

Route::view('/conversations', 'conversations.index')->name('conversations');
Route::get('/conversations/{user}', 'ConversationsController@index');
Route::post('/conversations/{user}', 'ConversationsController@store')->middleware('can:talkTo,user');
