<?php

Route::namespace('Api')->group(function () {
    Route::middleware('auth:api')->group(function ($router) {
        $router->get('/conversations', 'ConversationsController@index');
        $router->get('/conversations/{user}', 'ConversationsController@show')->middleware('can:talkTo,user');
        $router->post('/conversations/{user}', 'ConversationsController@store')->middleware('can:talkTo,user');
        $router->post('/messages/{message}', 'MessagesController@read')->middleware('can:read,message');
    });
});
